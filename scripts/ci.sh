#!/usr/bin/env bash

dir="$( dirname -- "$BASH_SOURCE"; )";

bash "$dir/setup.sh"
bash "$dir/build.sh"
bash "$dir/render.sh"
