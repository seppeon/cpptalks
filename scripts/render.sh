#!/usr/bin/env bash

export GEM_HOME="$(gem env user_gemhome)"
export PATH="$PATH:$GEM_HOME/bin"
export CWD="$(pwd)"

cmake -E rm public/*.svg

cd function_ref/slides
bundle exec asciidoctor-revealjs -D "${CWD}/public" -r asciidoctor-diagram function_ref.adoc
asciidoctor -D "${CWD}/public" algorithm_table.adoc

cd "$CWD"
cmake -E copy function_ref/slides/*.css public
cmake -E copy_directory function_ref/slides/plugins public/plugins
