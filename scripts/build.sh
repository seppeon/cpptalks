#!/usr/bin/env bash

cmake --no-warn-unused-cli -DCMAKE_C_COMPILER:FILEPATH=clang -DCMAKE_CXX_COMPILER:FILEPATH=clang++ -S. -B./build
cmake --build ./build --target OutputData
cmake -E make_directory public/graphs/code_sizes
cmake -E make_directory public/graphs/rate_of_change
cmake -E make_directory public/graphs/compile_time
cmake -E make_directory public/graphs/runtime
cp function_ref/slides/graphs/code_sizes/data.json public/graphs/code_sizes/data.json
cp function_ref/slides/graphs/rate_of_change/data.json public/graphs/rate_of_change/data.json
cp function_ref/slides/graphs/compile_time/data.json public/graphs/compile_time/data.json
cp function_ref/slides/graphs/runtime/data.json public/graphs/runtime/data.json
cp build/function_ref/*Symbols.txt public
