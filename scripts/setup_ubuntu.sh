#!/usr/bin/env bash

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install asciidoctor nodejs npm ninja-build python3 build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev tree npm git cmake gcc-14 clang-17 build-essential libc++-17-dev libc++abi-17-dev libstdc++-14-dev ruby-full -y
sudo npm install -g canvas vega-cli vega-lite
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-17 100
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-17 100
