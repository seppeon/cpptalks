#!/usr/bin/env bash

export GEM_HOME="$(gem env user_gemhome)"
export PATH="$PATH:$GEM_HOME/bin"
export CWD="$(pwd)"

cd function_ref/slides
bundle config --local path .bundle/gems
bundle
cd "$CWD"