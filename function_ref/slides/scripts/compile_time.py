import json
import sys
import time
import subprocess

def save_json(filename, contents):
	with open(filename, 'w') as f:
		json.dump(contents, f, indent=4)

out_file=sys.argv[1]
test=sys.argv[2]
tag=sys.argv[3]
args=sys.argv[4:]

start = time.time()
output = subprocess.run(args, text=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE).returncode
end = time.time()
delta = end - start

output_json = []
output_json.append({
    "test": test,
    "tag": tag,
    "time": delta,
})

save_json(out_file, output_json)
sys.exit(output)