import io
import os
import json
import argparse

def load_nums(filename):
	f = open(filename, 'r')
	output = []
	try:
		output = f.readlines()
		output = [float(i) for i in output]
	finally:
		f.close()
	return output

def save_json(filename, contents):
	with open(filename, 'w') as f:
		json.dump(contents, f, indent=4)

def as_json(object):
	return json.dumps(object, indent=2)

parser = argparse.ArgumentParser(description='Files to compile times.')
parser.add_argument('--test', type=str, nargs=1, help="The test of this input set.")
parser.add_argument('--tag', type=str, nargs=1, help="The tag of this input set.")
parser.add_argument('--input', type=str, nargs=1, help='Input file files that state compile time.')
parser.add_argument('--output', type=str, nargs=1, help='The file to accumulate tokens inside')
args = parser.parse_args()
test = args.test[0]
tag = args.tag[0]
input_file = args.input[0]

nums = load_nums(input_file)
output_json = []
for num in nums:
	output_json.append({
		"test": test,
		"tag": tag,
		"runtime": num,
	})
output_json_path = args.output[0]
save_json(output_json_path, output_json)