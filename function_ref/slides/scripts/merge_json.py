import json
import sys

out_file = sys.argv[1]
files=sys.argv[2:]

def merge(input_filenames, output_filename):
    result = list()
    for input_filename in input_filenames:
        with open(input_filename, 'r') as input_file:
            result.extend(json.load(input_file))

    with open(output_filename, 'w') as output_file:
        json.dump(result, output_file)

merge(files, out_file)
