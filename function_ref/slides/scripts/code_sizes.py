import re
import io
import os
import csv
import json
import argparse

def extract_code_sizes(matches, input):
	def match_regex(symbol_name, symbol_type, match_regexes, match_symbol_types):
		def has_symbol_type():
			for supported_symbol_type in match_symbol_types:
				if (str.lower(symbol_type) == str.lower(supported_symbol_type)):
					return True
			return False
		
		def symbol_matches_regex():
			for regex in match_regexes:
				if (bool(re.fullmatch(regex, symbol_name))):
					return True
			return False
		
		return has_symbol_type() and symbol_matches_regex()

	def parse_symbols_file(filename):
		output = {}
		with io.open(filename, mode="r", newline='', encoding="utf-8") as f:
			symbols_reader = csv.reader(f, delimiter=' ', quoting=csv.QUOTE_NONE)
			for row in symbols_reader:
				if (len(row) >= 4):
					tag = row[3]
					for i in range(4, len(row)):
						tag += ' ' + row[i]
					if not row[0].isnumeric():
						continue
					if not row[1].isnumeric():
						continue
					output[tag] = {
						'offset': int(row[0]),
						'size': int(row[1]),
						'type': str.lower(row[2]),
					}
		return output

	symbols_from_nm = parse_symbols_file(input)
	output = {}
	for symbol_from_nm in symbols_from_nm.items():
		symbol_name = symbol_from_nm[0]
		symbol_attributes = symbol_from_nm[1]
		symbol_type = symbol_attributes['type']
		for match in matches:
			match_regexes = match['regexes']
			match_symbol_types = match['symbol_type']
			if match_regex(symbol_name, symbol_type, match_regexes, match_symbol_types):
				match_category = match['category']
				symbol_offset = int(symbol_attributes['offset'])
				symbol_size = int(symbol_attributes['size'])
				output[symbol_name] = {
					'category': match_category,
					'offset': symbol_offset,
					'size': symbol_size,
				}
				break
	return output

def load_json(filename):
	f = open(filename)
	output = {}
	try:
		output = json.load(f)
	finally:
		f.close()
	return output

def save_json(filename, contents):
	with open(filename, 'w') as f:
		json.dump(contents, f, indent=4)

def as_json(object):
	return json.dumps(object, indent=2)

parser = argparse.ArgumentParser(description='Files to extract sizes.')
parser.add_argument('--test', type=str, nargs=1, help="The test of this input set.")
parser.add_argument('--tag', type=str, nargs=1, help="The tag of this input set.")
parser.add_argument('--input', type=str, nargs=1, help='Input file, nm output file (must include --print-sizes, in base 10).')
parser.add_argument('--config', type=str, nargs=1, help='Input config file, how to filter the nm log.')
parser.add_argument('--output', type=str, nargs=1, help='The file to accumulate tokens inside')
args = parser.parse_args()
test = args.test[0]
tag = args.tag[0]
total_size = 0
config = load_json(args.config[0])
matches = config['matches']
code_sizes = extract_code_sizes(matches, args.input[0])

total_size_by_category = {}
for key, code_size in code_sizes.items():
	total_size_by_category[code_size['category']] = 0
for key, code_size in code_sizes.items():
	total_size_by_category[code_size['category']] += code_size['size']

output_json_path = args.output[0]
output_json = []
for key, size in total_size_by_category.items():
	output_json.append({
		"test": test,
		"tag": tag,
		"category": key,
		"size": size,
	})
save_json(output_json_path, output_json)