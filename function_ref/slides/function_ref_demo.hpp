#include <functional>

template <auto V>
struct nontype_t{};
template <auto V>
inline constexpr auto nontype = nontype_t<V>{};

template <typename ...>
struct function_ref;

template <typename Ret, typename ... Args>
struct function_ref<Ret(Args...) cv> {
    template <typename Fn> requires (std::is_function_v<Fn>)
    constexpr function_ref(Fn * fn) noexcept
        : m_bound_entity{ .fn = reinterpret_cast<void (*)()>(fn) }
        , m_thunk(+[](BoundEntity bound_entity, Args... args) -> Ret {
            return std::invoke_r<Ret>(reinterpret_cast<Fn *>(bound_entity.fn), args...);
        })
    {}
    template <typename Obj>
    constexpr function_ref(Obj && obj) noexcept
        : m_bound_entity{ .cptr = std::addressof(obj) }
        , m_thunk(+[](BoundEntity bound_entity, Args... args) -> Ret {
            return std::invoke_r<Ret>(*static_cast<Obj const*>(bound_entity.cptr), args...);
        })
    {}
    template <auto V, typename Obj>
    constexpr function_ref(nontype_t<V>, Obj && obj) noexcept
        : m_bound_entity{ .cptr = std::addressof(obj) }
        , m_thunk(+[](BoundEntity bound_entity, Args... args) -> Ret {
            return std::invoke_r<Ret>(V, *static_cast<Obj const*>(bound_entity.cptr), args...);
        })
    {}
    template <auto V>
    constexpr function_ref(nontype_t<V>) noexcept
        : m_bound_entity{ .cptr = nullptr }
        , m_thunk(+[](BoundEntity, Args... args) -> Ret {
            return std::invoke_r<Ret>(V, args...);
        })
    {}

    [[nodiscard]] constexpr auto operator()(Args...args) const -> Ret {
        return m_thunk(m_bound_entity, args...);
    }
private:
    union BoundEntity {
        void const * cptr;
        void (*fn)();
    };
    BoundEntity m_bound_entity = {};
    Ret (*m_thunk)(BoundEntity bound_entity, Args...);
};