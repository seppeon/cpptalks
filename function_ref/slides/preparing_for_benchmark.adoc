== Code size breakdown example

`(✿'◠‿◠) ┻━┻ (◠‿◠▲)`

=== Lets build a thing!

The goal will be to:

* Break down the code size cost of various function wrappers, and
* Create a reusable library that can be used for compile time benchmarking.

[.columns]
=== Calculator

[.column]
* Arithmetic operations,
* Only integers.
* Function call syntax.

[.column]
[source,cpp]
----
add(1, 2)
sub(2,1)
mul(3, 5 )
div( 50, 5)
mod( 5 , 4 )
----

=== Code: Parsable

A `Parsable` is an object that has the `parse` function, it takes and returns a `std::string_view`:
[source,cpp]
----
template <typename T>
concept Parsable = requires(T const & object, std::string_view input) {
	{ object.parse(input) } -> std::same_as<std::string_view>;
};
----

=== Code: Helper Functions

Identify a string with a value as a `valid_string`:
[source,cpp]
----
constexpr bool valid_string(std::string_view input) {
	return input.data() != nullptr;
}
----

Do less then operation on the size of two `std::string_view`:
[source,cpp]
----
constexpr bool less_size(std::string_view lhs, std::string_view rhs) noexcept {
	return std::tuple{valid_string(lhs), lhs.size()} < std::tuple{valid_string(rhs), rhs.size()};
}
----
Get the result of parsing an input with several `Parsables`:
[source,cpp]
----
constexpr auto parse_all(std::string_view input, Parsable auto const & ... parsables) noexcept {
	return std::array<std::string_view, sizeof...(parsables)>{
		parsables.parse(input)
		....
	};
}
----

=== Code: Prefix

Eat a given prefix from a string:
[source,cpp]
----
struct Prefix {
	std::string_view prefix;

	auto parse(std::string_view input) const -> std::string_view {
		if (!input.starts_with(prefix)) { return {}; }
		input.remove_prefix(prefix.size());
		return input;
	}
};
----

=== Code: Whitespace

Eat any leading whitespace from a string:
[source, cpp]
----
struct Whitespaces {
	auto parse(std::string_view input) const -> std::string_view {
		size_t i = 0;
		for (; i < input.size(); ++i) {
			if (!IsSpace(input[i])) { break; }
		}
		return std::string_view{ input.begin() + i, input.end() };
	}
};
----

=== Code: Seq

Eat a series of other tokens:
[source, cpp]
----
template <Parsable ... Ts>
struct Seq {
	std::tuple<Ts...> elements;

	auto parse(std::string_view input) const -> std::string_view {
		auto const parse_one = [&](auto const & arg) {
			return valid_string(input = args.parse(input));
		};
		auto const parse_all = [&](Ts const & ... args) {
			return (parse_one(args) && ...) 
				? input : std::string_view{};
		};
		return apply(parse_all, elements);
	}
};
----

=== Code: Comma

Eat a comma a whitespaced comma:
[source, cpp]
----
struct Comma
{
	constexpr auto parse(std::string_view input) const -> std::string_view{
		return Seq{Whitespaces{}, Prefix{","}, Whitespaces{}}.parse(input);
	}
};
----

=== Code: Any

Greedily parse several `Parsable`:

[source,cpp]
----
template <Parsable ... Ts>
	requires (sizeof...(Ts) >= 1)
struct Any
{
	std::tuple<Ts...> elements;

	constexpr auto parse(std::string_view input) const -> std::string_view
	{
		return apply(
			[input](Ts const & ... args) -> std::string_view
			{
				const auto results = parse_all(input, args...)
				return *std::min_element(results.begin(), results.end(), less_size);
			},
			elements
		);
	}
};
----

=== Code: Identifying Our Value Types

Concept for any integer:
[source,cpp]
----
template <typename T>
concept AnyInt = AnyOf<
	T,
	unsigned char,      signed char,
	unsigned short,     signed short,
	unsigned int,       signed int,
	unsigned long int,  signed long int,
	unsigned long long, signed long long
>;
----

=== Code: Parsing Integers

Parsing integers:
[source,cpp]
----
template <typename T, CallableWith<T> F>
struct ValueParser;
template <AnyInt T, CallableWith<T> F>
struct ValueParser<T, F>
{
	F callable;
	int base = 10;

	constexpr auto parse(std::string_view input) const -> std::string_view
	{
		T value = {};
		auto const [ptr, ec] = std::from_chars(input.begin(), input.end(), value, base);
		if (ec != std::errc()) { return {}; }
		std::invoke(callable, value);
		return std::string_view{ ptr, input.end() };
	}
};
----

=== Code: Copy Assign Callable

Copy assign a value on invokation:
[source,cpp]
----
template <typename T>
struct CopyAssigner
{
	T & value;

	constexpr void operator()(T const & result) const {
		value = result;
	}
};
----

=== Code: Parse Tuple

Store each parsed element of a tuple, each seperated by a comma:

[source,cpp]
----
template <typename ... Args>
struct TupleParser<std::tuple<Args...>> {
	std::tuple<Args...> & args{};

	constexpr auto parse(std::string_view input) const -> std::string_view {
		auto const parse_arg = [&](size_t arg_index, auto & arg) {
			auto const arg_callable = CopyAssigner{ arg };
			input = ValueParser{ arg_callable }.parse(input);
			if (!valid_string(input)) { return false; }
			if ((arg_index + 1) != sizeof...(Args)) {
				input = Comma{}.parse(input);
			}
			return valid_string(input);
		};
		auto const handle_args = [&](auto & ... args) {
			auto i = size_t{0};
			return (parse_arg(i++, args) && ...);
		};
		if (!std::apply(handle_args, args)) { return {}; }
		return Whitespaces{}.parse(input);
	}
};
----

=== Code: Parse Function Arguments

Parse the arguments of a function and run a callback if the parse is successful:

[source,cpp]
----
template <typename F, typename Ret, typename ... Args>
struct ArgParser<F, Ret, std::tuple<Args...>> {
	F callback;
	Ret return_value;

	constexpr auto parse(std::string_view input) const -> std::string_view {
		std::tuple<Args...> args{};
		input = Seq{ 
			Prefix{"("}, Whitespaces{},
			TupleParser<std::tuple<Args...>>{args},
			Whitespaces{}, Prefix{")"}
		}.parse(input);
		if (!valid_string(input)) { return {}; }
		return_value(std::apply(callback, args));
		return input;
	}
};
----

=== Code: Parse a function call

Parse a function's arguments, call a callback with those arguments, provide the return value via a callback to the caller:

[source,cpp]
----
template <typename F, typename R>
struct CallableParser {
	using return_type = typename FnTraits<F>::ret;
	using args_type = typename FnTraits<F>::args;

	std::string_view name;
	F callable;
	R ret;

	constexpr auto parse(std::string_view input) const -> std::string_view {
		return Seq{ Prefix{name}, Whitespaces{}, ArgParser<F, R, args_type>{callable, ret} }.parse(input);
	}
};
----

=== Code: Operations

Each operations will be defined as follows:
[source,cpp]
----
auto const add = CallableParser{
	"add",
	FN([](int a, int b){ return a + b; }),
	FN([](int ret){ std::cout << "add: " << ret << "\n"; })
};
----

=== Entry point

Run several different parsers in order to include them in the program:

[source,cpp]
----
auto const any_parser = Any{ std::tuple{ add, sub, mul, div, mod } };
any_parser.parse("add(1, 2)");
any_parser.parse("sub(2, 3)");
any_parser.parse("mul(45, 2)");
any_parser.parse("div(123,3)");
any_parser.parse("mod(23, 3)");
----

=== Calculator: Code Size

vega::graphs/code_sizes/code_sizes.json[format=svg]