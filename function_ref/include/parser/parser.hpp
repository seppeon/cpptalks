#pragma once
#include "utils/fn_traits.hpp"
#include "utils/instantiations.hpp"
#include <algorithm>
#include <charconv>
#include <string_view>
#include <array>
#include <tuple>
#include <vector>

#define NOINLINE [[gnu::noinline]]

namespace Parser
{
	template <typename T>
	concept Parsable = requires(T const & object, std::string_view input)
	{
		{ object.parse(input) } -> std::same_as<std::string_view>;
	};

	template <typename T>
	concept NonVoid = !std::is_void_v<T>;

	template <typename F, typename T>
	concept CallableWith = requires(F && callback, T const & value) { { callback(value) }; };

	template <typename T, typename ... Args>
	concept AnyOf = (std::is_same_v<T, Args> || ...);

	[[nodiscard]] constexpr auto is_space(char input) -> bool
	{
		return (input == ' ') | (input == '\t');
	}

	[[nodiscard]] constexpr auto valid_string(std::string_view input) -> bool
	{
		UTILS_MARK_INSTANTIATION();
		return input.data() != nullptr;
	}

	struct Prefix
	{
		std::string_view prefix;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			if (!input.starts_with(prefix))
			{
				return {};
			}
			input.remove_prefix(prefix.size());
			return input;
		}
	};

	struct Whitespaces // zero or more
	{
		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			size_t i = 0;
			for (; i < input.size(); ++i)
			{
				if (!is_space(input[i]))
				{
					break;
				}
			}
			return std::string_view{ input.begin() + i, input.end() };
		}
	};

	template <Parsable ... Ts>
	struct Seq
	{
		std::tuple<Ts...> elements;

		constexpr Seq() = default;
		constexpr Seq(Ts const & ... ts)
			: elements(ts...)
		{}

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			return apply(
				[&](Ts const & ... args) -> std::string_view
				{
					UTILS_MARK_INSTANTIATION();
					auto output = input;
					return (
						(valid_string(output = args.parse(output)) && ...)
						? output
						: std::string_view{}
					);
				},
				elements
			);
		}
	};
	template <typename ... Args>
	Seq(Args...)->Seq<Args...>;

	struct Comma
	{
		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view{
			UTILS_MARK_INSTANTIATION();
			return Seq{Whitespaces{}, Prefix{","}, Whitespaces{}}.parse(input);
		}
	};

	template <Parsable ... Ts>
		requires (sizeof...(Ts) >= 1)
	struct Any
	{
		std::tuple<Ts...> elements;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			return apply(
				[&](Ts const & ... args) -> std::string_view
				{
					UTILS_MARK_INSTANTIATION();
					const auto results = std::array<std::string_view, sizeof...(Ts)>{
						args.parse(input)
						...
					};
					return *std::max_element(results.begin(), results.end(), [](std::string_view lhs, std::string_view rhs){
						UTILS_MARK_INSTANTIATION();
						return lhs.size() < rhs.size();
					});
				},
				elements
			);
		}
	};
	template <Parsable ... Ts>
	Any(std::tuple<Ts...>)->Any<Ts...>;

	template <typename T>
	concept AnyInt = AnyOf<
		T,
		unsigned char,      signed char,
		unsigned short,     signed short,
		unsigned int,       signed int,
		unsigned long int,  signed long int,
		unsigned long long, signed long long
	>;

	template <typename T>
	concept AnyFloat = AnyOf<T, float, double>;

	template <typename T, CallableWith<T> F>
	struct ValueParser;
	template <AnyInt T, CallableWith<T> F>
	struct ValueParser<T, F>
	{
		F callable;
		int base = 10;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			T value = {};
			auto const [ptr, ec] = std::from_chars(input.begin(), input.end(), value, base);
			if (ec != std::errc()) { return {}; }
			callable(value);
			return std::string_view{ ptr, input.end() };
		}
	};
	template <AnyFloat T, CallableWith<T> F>
	struct ValueParser<T, F>
	{
		F callable;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			T value = {};
			auto const [ptr, ec] = std::from_chars(input.begin(), input.end(), value);
			if (ec != std::errc()) { return {}; }
			callable(value);
			return std::string_view{ ptr, input.end() };
		}
	};
	template <CallableWith<bool> F>
	struct ValueParser<bool, F>
	{
		F callable;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			if (input.starts_with("true"))
			{
				callable(true);
				return input.substr(4);
			}
			if (input.starts_with("false"))
			{
				callable(false);
				return input.substr(5);
			}
			return {};
		}
	};
	template <typename F>
	ValueParser(F)->ValueParser<std::remove_cvref_t<std::tuple_element_t<0, typename Utils::FnTraits<F>::args>>, F>;
	template <typename F>
	ValueParser(F, int)->ValueParser<std::remove_cvref_t<std::tuple_element_t<0, typename Utils::FnTraits<F>::args>>, F>;

	template <typename T>
	struct CopyAssigner
	{
		T & value;
		NOINLINE constexpr void operator()(T const & result) const
		{
			UTILS_MARK_INSTANTIATION();
			value = result;
		}
	};

	template <typename T>
	struct TupleParser;
	template <typename ... Args>
	struct TupleParser<std::tuple<Args...>>
	{
		std::tuple<Args...> & args;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view {
			auto const parse_arg = [&](size_t arg_index, auto & arg) {
				UTILS_MARK_INSTANTIATION();
				auto const arg_callable = CopyAssigner{ arg };
				input = ValueParser{ arg_callable }.parse(input);
				if (!valid_string(input)) { return false; }
				if ((arg_index + 1) != sizeof...(Args)) {
					input = Comma{}.parse(input);
				}
				return valid_string(input);
			};
			auto const handle_args = [&](auto & ... vs) {
				UTILS_MARK_INSTANTIATION();
				auto i = size_t{0};
				return (parse_arg(i++, vs) && ...);
			};
			UTILS_MARK_INSTANTIATION();
			if (!std::apply(handle_args, args)) { return {}; }
			return Whitespaces{}.parse(input);
		}
	};


	template <typename F, typename Ret, typename T>
	struct ArgParser;
	template <typename F, typename Ret, typename ... Args>
	struct ArgParser<F, Ret, std::tuple<Args...>>
	{
		F callback;
		Ret return_value;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view {
			UTILS_MARK_INSTANTIATION();
			std::tuple<Args...> args{};
			input = Seq{ 
				Prefix{"("},
				Whitespaces{},
				TupleParser<std::tuple<Args...>>{args},
				Whitespaces{},
				Prefix{")"}
			}.parse(input);
			if (!valid_string(input)) { return {}; }
			return_value(std::apply(callback, args));
			return input;
		}
	};

	template <typename F, typename R>
	struct CallableParser {
		using return_type = typename Utils::FnTraits<F>::ret;
		using args_type = typename Utils::FnTraits<F>::args;

		std::string_view name;
		F callable;
		R ret;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view
		{
			UTILS_MARK_INSTANTIATION();
			return Seq{ Prefix{name}, Whitespaces{}, ArgParser<F, R, args_type>{callable, ret} }.parse(input);
		}
	};

	struct ErasedCallableParser {
		template <typename F, typename R>
		ErasedCallableParser(CallableParser<F, R> const & callable_parser)
			: m_obj_ptr(&callable_parser)
			, m_obj_fn(+[](void const * obj_ptr, std::string_view input) -> std::string_view {
				return static_cast<CallableParser<F, R> const *>(obj_ptr)->parse(input);
			})
		{}

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view {
			return m_obj_fn(m_obj_ptr, input);
		}
	private:
		void const * m_obj_ptr = nullptr;
		std::string_view (*m_obj_fn)(void const *, std::string_view input) = nullptr;
	};

	struct ErasedAnyParser {
		std::vector<ErasedCallableParser> parsers;

		NOINLINE constexpr auto parse(std::string_view input) const -> std::string_view {
			for (auto const & parser : parsers) {
				auto const parse_result = parser.parse(input);
				if (valid_string(parse_result)) {
					return parse_result;
				}
			}
			return {};
		}
	};
}