#include "parser/parser.hpp"
#include "utils/instantiations.hpp"
#include "utils/fn_traits.hpp"
#include <string_view>
#include <iostream>
#include <chrono>

#ifdef STD_FUNCTION
#include <functional>
#define FN(x) std::function(x)
#elifdef STD_FUNCTION_REF
#include "function_ref.hpp"
#define FN(x) tl::function_ref<typename Utils::FnTraits<std::remove_const_t<decltype(x)>>::sig>(x)
#else
#define FN(x) x
#endif