#pragma once
namespace Utils
{
	inline int & InstantiationCount()
	{
		static int value = 0;
		return value;
	}
}
#define UTILS_MARK_INSTANTIATION() { if !consteval { static int count = ::Utils::InstantiationCount()++; } }