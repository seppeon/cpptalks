#pragma once
#include <tuple>
#include <type_traits>

namespace Utils
{
	template<typename Obj>
	concept FunctionOperator = requires
	{
		{ &std::remove_cvref_t<Obj>::operator() };
	};

	template <typename ... Args>
	using ArgList = std::tuple<Args...>;

	template <typename T>
	struct FnTraits;
	template <FunctionOperator T>
	struct FnTraits<T> : FnTraits<decltype( &std::remove_cvref_t<T>::operator() )>
	{
	};
	template <typename Ret, typename ... Args>
	struct FnTraits<Ret(Args...)>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename ... Args>
	struct FnTraits<Ret(*)(Args...)>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename ... Args>
	struct FnTraits<Ret(&)(Args...)>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...)>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) volatile>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const volatile>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) &>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const &>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) volatile &>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const volatile &>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) &&>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const &&>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) volatile &&>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const volatile &&>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename ... Args>
	struct FnTraits<Ret(Args...) noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename ... Args>
	struct FnTraits<Ret(*)(Args...) noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename ... Args>
	struct FnTraits<Ret(&)(Args...) noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) const noexcept;
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) volatile noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) const noexcept;
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const volatile noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) const volatile noexcept;
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) & noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const & noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) const noexcept;
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) volatile & noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) volatile noexcept;
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const volatile & noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) const volatile noexcept;
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) && noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const && noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) volatile && noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...);
	};
	template <typename Ret, typename Obj, typename ... Args>
	struct FnTraits<Ret (Obj::*)(Args...) const volatile && noexcept>
	{
		using ret = Ret;
		using args = ArgList<Args...>;
		using sig = ret(Args...) const volatile noexcept;
	};
}