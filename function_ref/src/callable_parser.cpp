#include "gen/common_header.hpp"
#include <iostream>

int main()
{
	auto const add = Parser::CallableParser{
		"add",
		FN([](int a, int b){ return a + b; }),
		FN([](int ret){ std::cout << "add: " << ret << "\n"; })
	};
	auto const sub = Parser::CallableParser{
		"sub",
		FN([](int a, int b){ return a - b; }),
		FN([](int ret){ std::cout << "sub: " << ret << "\n"; })
	};
	auto const mul = Parser::CallableParser{
		"mul",
		FN([](int a, int b){ return a * b; }),
		FN([](int ret){ std::cout << "mul: " << ret << "\n"; })
	};
	auto const div = Parser::CallableParser{
		"div",
		FN([](int a, int b){ return a / b; }),
		FN([](int ret){ std::cout << "div: " << ret << "\n"; })
	};
	auto const mod = Parser::CallableParser{
		"mod",
		FN([](int a, int b){ return a % b; }),
		FN([](int ret){ std::cout << "mod: " << ret << "\n"; })
	};
	auto const any_parser = Parser::Any{ std::tuple{ add, sub, mul, div, mod } };
	any_parser.parse("add(1, 2)");
	any_parser.parse("sub(2, 3)");
	any_parser.parse("mul(45, 2)");
	any_parser.parse("div(123,3)");
	any_parser.parse("mod(23, 3)");
	return Utils::InstantiationCount();
}