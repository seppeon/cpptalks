#include <iostream>

[[gnu::noinline]] void foo(auto a, auto b) {
	a();
	b();
}

int main() {
	foo( []{ std::cout << "hello, "; }, []{ std::cout << "world!\n"; } );
	foo( []{ std::cout << "hello, "; }, []{ std::cout << "world!\n"; } );
}