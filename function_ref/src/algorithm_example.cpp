#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8};
    auto const sum = std::ranges::fold_left(
        v, 0,
        [](int a, int b) { return a + b; }
    );
    std::cout << "sum: " << sum << "\n";
}