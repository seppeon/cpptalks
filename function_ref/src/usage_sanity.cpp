#include "std23/function_ref.h"
#include <string>
#include <iostream>

struct Lifetime {
    Lifetime() {
        std::puts("Begin");
    }
    ~Lifetime() {
        std::puts("End");
    }
};

auto return_given_function(std23::function_ref<std::string(std::string)> type) -> std23::function_ref<std::string(std::string)> {
    return type;
}

int main() {
    auto const fn_ref = return_given_function([a = std::string(), b = Lifetime()](std::string value) mutable{
        a += value;
        return a; 
    });
    std::cout << fn_ref("hello this is a long message") << "\n";
    std::cout << fn_ref(", world") << "\n";
}
