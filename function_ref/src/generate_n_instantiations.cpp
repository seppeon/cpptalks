#include <algorithm>
#include <cctype>
#include <span>
#include <cstdint>
#include <filesystem>
#include <iostream>
#include <memory>
#include <ostream>
#include <random>
#include <stdexcept>
#include <variant>
#include <vector>
#include <string>
#include <fstream>

namespace {
auto ExtractArgs(int argc, const char * const * argv) -> std::vector<std::string> {
	return std::vector<std::string>{ argv + std::min<int>(1, argc), argv + argc };
}
auto IsUnsignedNumber(std::string const & arg) -> bool {
	auto const is_digit = +[](char c){ return ('0' <= c) && (c <= '9'); };
	return std::all_of(arg.begin(), arg.end(), is_digit);
}
auto FindRequiredArgIndex(std::vector<std::string> const & args, std::string const & arg) {
	auto const it = std::find(args.begin(), args.end(), arg);
	if (it == args.end()) {
		throw std::runtime_error("At least one " + arg + " <number>... argument must be present!");
	}
	return static_cast<size_t>(it - args.begin());
}
auto ExtractNumbers(std::vector<std::string> const & args) -> std::vector<uint32_t> {
	std::vector<uint32_t> output;
	auto const start_index = FindRequiredArgIndex(args, "-N");
	for (auto i = start_index + 1; i < args.size(); ++i) {
		auto const & arg = args[i];
		if (!IsUnsignedNumber(arg)) {
			break;
		}
		output.push_back(static_cast<uint32_t>(atoi(arg.data())));
	}
	if (output.empty()) {
		throw std::runtime_error("At least 1 number argument proceeding the -N argument, defining the number of callables to generate!");
	}
	return output;
}
auto ExtractNumberArg(std::vector<std::string> const & args, std::string match) -> uint32_t {
	std::vector<uint32_t> output;
	auto const start_index = FindRequiredArgIndex(args, match);
	for (auto i = start_index + 1; i < args.size(); ++i) {
		auto const & arg = args[i];
		if (!IsUnsignedNumber(arg)) {
			break;
		}
		return static_cast<uint32_t>(atoi(arg.data()));
	}
	throw std::runtime_error("1 number argument must proceed the " + match + " argument!");
}
auto ExtractMinArgs(std::vector<std::string> const & args) -> uint32_t {
	return ExtractNumberArg(args, "-M");
}
auto ExtractMaxArgs(std::vector<std::string> const & args) -> uint32_t {
	return ExtractNumberArg(args, "-X");
}
auto ExtractMaxDataMembers(std::vector<std::string> const & args) -> uint32_t {
	return ExtractNumberArg(args, "-D");
}
auto ExtractMaxExpressionDepth(std::vector<std::string> const & args) -> uint32_t {
	return ExtractNumberArg(args, "-K");
}
auto ExtractWorkingDirectory(std::vector<std::string> const & args) -> std::filesystem::path {
	auto const start_index = FindRequiredArgIndex(args, "-W");
	for (auto i = start_index + 1; i < args.size(); ++i) {
		auto const & arg = args[i];
		if (std::filesystem::exists(arg) and std::filesystem::is_directory(arg)) {
			return arg;
		}
		throw std::runtime_error("The path to the base directory must be provided after -W argument!");
	}
	throw std::runtime_error("The -W argument is required, and must define an existing directory!");
}
auto ExtractFilenamePrefix(std::vector<std::string> const & args) -> std::string {
	auto const start_index = FindRequiredArgIndex(args, "-P");
	for (auto i = start_index + 1; i < args.size(); ++i) {
		return args[i];
	}
	throw std::runtime_error("The -P argument is required!");
}
auto GetFilename(std::filesystem::path const & working_directory, std::string const & filename, uint32_t instantiations) -> std::filesystem::path {
	return working_directory / (filename + "_" + std::to_string(instantiations) + ".cpp");
}
auto GetOutputFile(std::vector<std::string> const & args) -> std::string {
	auto const start_index = FindRequiredArgIndex(args, "-O");
	for (auto i = start_index + 1; i < args.size(); ++i) {
		return args[i];
	}
	throw std::runtime_error("The -O argument is required!");
}

struct GenerationJob {
	uint32_t instantiations = 0;
	std::filesystem::path destination = "";	
};
std::ostream & operator<<(std::ostream & s, GenerationJob const & job) {
	return (s << "with " << job.instantiations << " instantiations @ " << job.destination);
}
auto GetGenerationJobs(std::filesystem::path const & working_directory, std::string const & filename, std::span<uint32_t const> number_of_instantiations) -> std::vector<GenerationJob> {
	std::vector<GenerationJob> output;
	for (auto const & number_of_instantiation : number_of_instantiations) {
		output.push_back(GenerationJob{number_of_instantiation, GetFilename(working_directory, filename, number_of_instantiation)});
	}
	return output;
}

struct InfixExpression;

using Operand = std::variant<std::string, std::unique_ptr<InfixExpression>>;

struct InfixExpression {
	char op = '\0';
	Operand lhs = {};
	Operand rhs = {};
};

template<typename... Ts>
struct Overloads : Ts...{ using Ts::operator()...; };
template<typename... Ts>
Overloads(Ts...) -> Overloads<Ts...>;

struct InfixExpressionRenderer {
	std::string operator()(InfixExpression const & expression) const {
		return "(" + (*this)(expression.lhs) + expression.op + (*this)(expression.rhs) + ")";
	}
	std::string operator()(Operand const & expression) const {
		return visit(
			Overloads{
				[](std::string const & identifier) {
					return identifier;
				},
				[this](std::unique_ptr<InfixExpression> const & infix_expression) {
					return (*this)(*infix_expression);
				}
			},
			expression
		);
	}
};
inline constexpr auto infix_expression_renderer = InfixExpressionRenderer{};

enum class CallableType : uint32_t {
	Lambda = 0,
	Functor = 1,
	FreeFunction = 2,
};
enum class ValueType {
	Int = 0,
	Float = 1,
};

struct Generator {
	virtual ~Generator(){}
	virtual auto Define(std::string name, ValueType type) const -> std::string = 0;
	virtual auto Instantiate(std::string name, ValueType type) const -> std::string = 0;
	virtual auto Test(std::string name, ValueType type) const -> std::string = 0;
};
auto GetTypeName(ValueType type) -> std::string {
	switch (type) {
	case ValueType::Int: return "int";
	case ValueType::Float: return "float";
	default: std::terminate();
	}
}
auto GetRandomValue(ValueType type) -> std::string {
	switch (type) {
	case ValueType::Int: return std::to_string(1 + rand());
	case ValueType::Float: return std::to_string(1 + rand());
	default: std::terminate();
	}
}
auto GetRandomCallableType(uint32_t data_member_count) -> CallableType {
	auto const count = 2 + (data_member_count == 0);
	return CallableType(rand() % count);
}
auto GetRandomValueType() -> ValueType {
	return ValueType(rand() % 2);
}
auto RandomIndexBetween(std::random_device & random_device, std::size_t min, std::size_t max) -> size_t {
	auto engine = std::mt19937(random_device());
	auto dist = std::uniform_int_distribution<size_t>(min, max);
	return dist(engine);
}

struct Functor : Generator {
	auto Define(std::string name, ValueType type) const -> std::string override {
		std::string output;
		output += "struct " + name + " {\n";
		for (auto const & data_member : m_data_members) {
			output += "\t" + GetTypeName(type) + " " + data_member + " = " + GetRandomValue(type) + ";\n";
		}
		output += "\n";
		output += "\tauto operator()(";
		if (!m_args.empty()) {
			for (size_t i = 0; i < m_args.size(); ++i) {
				output += GetTypeName(type) + " " + m_args[i];
				if (i != m_args.size() - 1) {
					output += ", ";
				}
			}
		}
		output += ") const {\n";
		output += "\t\treturn " + infix_expression_renderer(*m_expression) + ";\n";
		output += "\t}\n";
		output += "};";
		return output;
	}
	auto Instantiate(std::string name, ValueType type) const -> std::string override {
		return name + "{}";
	}
	auto Test(std::string name, ValueType type) const -> std::string override {
		std::string output;
		output += name + "(";
		for (auto const & arg : m_args) {
			output += GetRandomValue(type) + ",";
		}
		if (!m_args.empty()) {
			output.pop_back();
		}
		output += ")";
		return output;
	}

	Functor(std::vector<std::string> data_members,std::vector<std::string> args,std::unique_ptr<InfixExpression> expression)
		: m_data_members(std::move(data_members))
		, m_args(std::move(args))
		, m_expression(std::move(expression))
	{}

	std::vector<std::string> m_data_members;
	std::vector<std::string> m_args;
	std::unique_ptr<InfixExpression> m_expression;
};
struct Lambda : Generator {
	auto Define(std::string name, ValueType type) const -> std::string override {
		return "";
	}
	auto Instantiate(std::string name, ValueType type) const -> std::string override {
		std::string output;
		output += "[";
		if (!m_data_members.empty()) {
			for (size_t i = 0; i < m_data_members.size(); ++i) {
				auto const & data_member = m_data_members[i];
				output += data_member + " = " + GetTypeName(type) + "(" + GetRandomValue(type) + ")";
				if (i != m_data_members.size() - 1) {
					output += ", ";
				}
			}
		}
		output += "](";
		if (!m_args.empty()) {
			for (size_t i = 0; i < m_args.size(); ++i) {
				output += GetTypeName(type) + " " + m_args[i];
				if (i != m_args.size() - 1) {
					output += ", ";
				}
			}
		}
		output += "){\n";
		output += "\treturn " + infix_expression_renderer(*m_expression) + ";\n";
		output += "}";
		return output;
	}
	auto Test(std::string name, ValueType type) const -> std::string override {
		std::string output;
		output += name + "(";
		for (auto const & arg : m_args) {
			output += GetRandomValue(type) + ",";
		}
		if (!m_args.empty()) {
			output.pop_back();
		}
		output += ")";
		return output;
	}

	Lambda(	std::vector<std::string> data_members,std::vector<std::string> args,std::unique_ptr<InfixExpression> expression)
		: m_data_members(std::move(data_members))
		, m_args(std::move(args))
		, m_expression(std::move(expression))
	{}

	std::vector<std::string> m_data_members;
	std::vector<std::string> m_args;
	std::unique_ptr<InfixExpression> m_expression;
};
struct FreeFunction : Generator {
	auto Define(std::string name, ValueType type) const -> std::string override {
		std::string output;
		output += "auto " + name + "(";
		if (!m_args.empty()) {
			for (size_t i = 0; i < m_args.size(); ++i) {
				output += GetTypeName(type) + " " + m_args[i];
				if (i != m_args.size() - 1) {
					output += ", ";
				}
			}
		}
		output += ") -> " + GetTypeName(type) + " {\n";
		output += "\treturn " + infix_expression_renderer(*m_expression) + ";\n";
		output += "}\n";
		return output;
	}
	auto Instantiate(std::string name, ValueType type) const -> std::string override {
		return "&" + name;
	}
	auto Test(std::string name, ValueType type) const -> std::string override {
		std::string output;
		output += name + "(";
		for (auto const & arg : m_args) {
			output += GetRandomValue(type) + ",";
		}
		if (!m_args.empty()) {
			output.pop_back();
		}
		output += ")";
		return output;
	}

	FreeFunction(std::vector<std::string> args,std::unique_ptr<InfixExpression> expression)
		: m_args(std::move(args))
		, m_expression(std::move(expression))
	{}

	std::vector<std::string> m_args;
	std::unique_ptr<InfixExpression> m_expression;
};

struct RandomExpression {
	explicit RandomExpression(uint32_t min_arg_count, uint32_t max_arg_count, uint32_t max_data_member_count)
		: m_min_arg_count(min_arg_count)
		, m_max_arg_count(max_arg_count)
		, m_max_data_member_count(max_data_member_count)
	{}

	[[nodiscard]] auto GetRandomInfixExpression(uint32_t max_depth) -> std::unique_ptr<Generator> {
		uint32_t arg_number = 0;
		auto arg_count = RandomIndexBetween(m_random_device, m_min_arg_count, m_max_arg_count);
		auto const data_member_count = RandomIndexBetween(m_random_device, 0, m_max_data_member_count + 1);
		if (!arg_count && !data_member_count) {
			++arg_count;
		}
		std::vector<std::string> identifiers;
		std::vector<std::string> args;
		for (uint32_t i = 0; i < arg_count; ++i) {
			identifiers.emplace_back(args.emplace_back(CreateNumberedIdentifier(arg_number++)));
		}
		std::vector<std::string> data_members;
		for (uint32_t i = 0; i < data_member_count; ++i) {
			identifiers.emplace_back(data_members.emplace_back(CreateNumberedIdentifier(arg_number++)));
		}
		switch (GetRandomCallableType(data_member_count)) {
		case CallableType::FreeFunction:
			return std::make_unique<FreeFunction>(args, GetRandomExpression(identifiers, arg_count, data_member_count, max_depth));
		case CallableType::Functor:
			return std::make_unique<Functor>(data_members, args, GetRandomExpression(identifiers, arg_count, data_member_count, max_depth));
		case CallableType::Lambda:
			return std::make_unique<Lambda>(data_members, args, GetRandomExpression(identifiers, arg_count, data_member_count, max_depth));
		default:
			std::terminate();
		}
	}
private:
	auto GetRandomElement(auto const & container) {
		return container[RandomIndexBetween(m_random_device, 0, std::size(container) - 1)];
	}
	auto GetRandomExpression(std::vector<std::string> const & identifiers, uint32_t arg_count, uint32_t data_member_count, uint32_t max_depth) -> std::unique_ptr<InfixExpression> {
		if (max_depth == 0) {
			return std::make_unique<InfixExpression>(
				GetRandomOperation(),
				GetRandomIdentifier(identifiers),
				GetRandomIdentifier(identifiers)
			);
		}
		auto operation = GetRandomOperation();
		auto lhs = GetRandomOperand(identifiers, arg_count, data_member_count, max_depth - 1);
		auto rhs = GetRandomOperand(identifiers, arg_count, data_member_count, max_depth - 1);
		return std::make_unique<InfixExpression>(std::move(operation), std::move(lhs), std::move(rhs));
	}
	auto GetArgs(std::vector<std::string> const & identifiers, uint32_t arg_count) const -> std::vector<std::string> {
		std::vector<std::string> output;
		for (size_t i = 0; i < arg_count; ++i) {
			output.push_back(identifiers[i]);
		}
		return output;
	}
	auto GetDataMembers(std::vector<std::string> const & identifiers, uint32_t arg_count, uint32_t data_member_count) const -> std::vector<std::string> {
		std::vector<std::string> output;
		for (size_t i = 0; i < data_member_count; ++i) {
			output.push_back(identifiers[i + arg_count]);
		}
		return output;
	}
	auto GetRandomIdentifier(std::vector<std::string> const & identifiers) -> std::string {
		return GetRandomElement(identifiers);
	}
	auto GetRandomOperation() -> char {
		return GetRandomElement(std::string_view("+-*"));
	}
	auto GetRandomOperand(std::vector<std::string> const & identifiers, uint32_t arg_count, uint32_t data_member_count, uint32_t max_depth) -> Operand {
		if (max_depth == 0) {
			return GetRandomIdentifier(identifiers);
		}
		if (RandomIndexBetween(m_random_device, 0, 1) == 0) {
			return GetRandomIdentifier(identifiers);
		} else {
			return GetRandomExpression(identifiers, arg_count, data_member_count, max_depth - 1);
		}
	}
	static auto CreateNumberedIdentifier(uint32_t number) -> std::string {
		return "id_" + std::to_string(number);
	}

	std::random_device m_random_device;
	uint32_t m_min_arg_count = 0;
	uint32_t m_max_arg_count = 0;
	uint32_t m_max_data_member_count = 0;
};
}

int main(int argc, const char * const * argv) {
	auto const args = ExtractArgs(argc, argv);
	auto const working_directory = ExtractWorkingDirectory(args);
	auto const filename_prefix = ExtractFilenamePrefix(args);
	auto const min_args = ExtractMinArgs(args);
	auto const max_args = ExtractMaxArgs(args);
	auto const max_expression_depth = ExtractMaxExpressionDepth(args);
	auto const max_data_members = ExtractMaxDataMembers(args);
	auto const generation_jobs = GetGenerationJobs(working_directory, filename_prefix, ExtractNumbers(args));

	std::cout << "Generating requested files:\n";
	auto random_expression = RandomExpression(min_args, max_args, max_data_members);
	for (auto const [number_of_callable, output_path] : generation_jobs) {
		auto output_stream = std::ofstream(output_path);
		output_stream << "#include \"gen/common_header.hpp\"\n\n";

		std::vector<std::string> tests;
		std::cout << "Generating definitions and instantiations of callables...\n";
		for (uint32_t i = 0; i < number_of_callable; ++i) {
			auto const infix_expression = random_expression.GetRandomInfixExpression(max_expression_depth);
			auto const value_type = GetRandomValueType();
			auto const name = "Callable" + std::to_string(i);
			auto const definition = infix_expression->Define(name, value_type);
			auto const instantiate = infix_expression->Instantiate(name, value_type);
			auto const test = infix_expression->Test(name, value_type);
			auto const variable = "var_" + std::to_string(i);
			auto const variable_result = "var_result_" + std::to_string(i);
			if (!definition.empty()) {
				output_stream << definition << "\n";
			}
			output_stream << "auto const " << variable << " = " << instantiate << ";\n";
			output_stream << "auto const " << variable_result << " = [](" + GetTypeName(value_type) + " ret){ volatile " + GetTypeName(value_type) + " v; v = ret; };\n\n";
			tests.push_back(test);
		}

		std::cout << "Generating parser from instantiations...\n";
		for (uint32_t i = 0; i < number_of_callable; ++i) {
			auto const name = "Callable" + std::to_string(i);
			auto const variable = "var_" + std::to_string(i);
			auto const variable_result = "var_result_" + std::to_string(i);
			auto const callable_parser = "parser_" + std::to_string(i);
			auto const type_erased_callable_parser = "type_erased_parser_" + std::to_string(i);
			output_stream << "auto const " << callable_parser << " = Parser::CallableParser{\n";
			output_stream << "\t\"" << name << "\",\n";
			output_stream << "\tFN(" << variable << "),\n";
			output_stream << "\tFN(" << variable_result << "),\n";
			output_stream << "};\n";
			output_stream << "auto const " << type_erased_callable_parser << " = Parser::ErasedCallableParser{" + callable_parser + "};\n\n";
		}

		std::cout << "Combining all type erased parsers into one parser...\n";
		output_stream << "int main(){\n";
		output_stream << "\tauto combined_parser = Parser::ErasedAnyParser{};\n";
		for (uint32_t i = 0; i < number_of_callable; ++i) {
			auto const type_erased_callable_parser = "type_erased_parser_" + std::to_string(i);
			output_stream << "\tcombined_parser.parsers.push_back(" << type_erased_callable_parser << ");\n";
		}

		std::cout << "Generating the loop that takes console input...\n";
		output_stream << "\tstd::string_view tests[]{\n";
		for (auto const & test : tests) {
			output_stream << "\t\t\"" << test << "\",\n";
		}
		output_stream << "\t};\n";
		output_stream << "\tauto const start = std::chrono::steady_clock::now();\n";
		output_stream << "\tfor (auto const & test : tests) {\n";
		output_stream << "\t\t(void)combined_parser.parse(test);\n";
		output_stream << "\t}\n";
		output_stream << "\tauto const end = std::chrono::steady_clock::now();\n";
		output_stream << "\tauto const diff = end - start;\n";
		output_stream << "\tstd::cout << std::chrono::duration<double, std::milli>(diff).count() << std::endl;\n";
		output_stream << "\treturn 0;\n";
		output_stream << "}\n";

		std::cout << "Saved generated file to " << output_path << "\n";
	}
}