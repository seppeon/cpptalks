#include <iostream>

template <typename A, typename B>
[[gnu::noinline]] void print_csv_row(A a, B b)
{
	std::cout << a << ", " << b << "\n";
}

int main()
{
	print_csv_row(1000, 1000);
	print_csv_row('a', 1000);
	print_csv_row(1000, 'a');
	print_csv_row('a', 'a');
}